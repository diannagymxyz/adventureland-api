import { ChainId, Token } from "@pancakeswap-libs/sdk";

// BEP-20 addresses.
export const CAKE = "0xc59824a2AB5DB97B8202E283fd1b8584c69348d0";
export const WBNB = "0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c";
export const DEAD = "0x000000000000000000000000000000000000dEaD";

// Contract addresses.
export const CAKE_BNB_FARM = "0xe4ea7fcfc9d34bc2cf4c852c48210653922ef043";
export const MASTERCHEF_CONTRACT = "0x0B8571BF2d64B842A8bE127A09A36f78E5151875";
export const LOTTERY_CONTRACT = "0xF98CB7f82C36f4205737aC9F27306C5C49B7A082";
export const MULTICALL_CONTRACT = "0x1Ee38d535d541c55C9dae27B12edf090C608E6Fb";

// PancakeSwap SDK Token.
export const CAKE_TOKEN = new Token(ChainId.MAINNET, CAKE, 18);
export const WBNB_TOKEN = new Token(ChainId.MAINNET, WBNB, 18);
export const CAKE_BNB_TOKEN = new Token(ChainId.MAINNET, CAKE_BNB_FARM, 18);
